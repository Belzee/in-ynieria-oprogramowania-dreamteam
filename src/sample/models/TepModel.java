package sample.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
/**
 * Tymczasowa klasa symbolizujaca wyglad kazdego modelu
 * Model to podstawowa jednostka danych
 * Nie zapewnia logiki
 * Jest kontenerem przchowujacym dane odpowiadajace potrzebom projektowym
 */
public class TepModel {

}
